EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1150 3750 3400 1700
U 5FECEB13
F0 "PowerSupply" 50
F1 "PowerSupply.sch" 50
$EndSheet
$Sheet
S 1200 1200 3350 1950
U 5FED8DE7
F0 "MainCircuit" 50
F1 "MainCircuit.sch" 50
$EndSheet
$Sheet
S 5350 1200 4000 1950
U 603282AC
F0 "Connectors" 50
F1 "Connectors.sch" 50
$EndSheet
$EndSCHEMATC
