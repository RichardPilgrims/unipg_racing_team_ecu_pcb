EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4150 3350 0    40   ~ 0
1%
Text Notes 3950 2100 0    40   ~ 0
2.0A\n
Text Notes 5700 3100 0    50   ~ 0
(1.4MHz)
Wire Wire Line
	4500 2550 4500 2650
Wire Wire Line
	4350 3400 4350 3550
$Comp
L power:GNDREF #PWR03
U 1 1 60028EAD
P 4350 3550
F 0 "#PWR03" H 4350 3300 50  0001 C CNN
F 1 "GNDREF" H 4355 3377 50  0001 C CNN
F 2 "" H 4350 3550 50  0001 C CNN
F 3 "" H 4350 3550 50  0001 C CNN
	1    4350 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 6000ACA9
P 4700 2600
F 0 "C3" H 4750 2700 50  0000 L CNN
F 1 "0.1uF" H 4800 2600 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 4700 2600 50  0001 C CNN
F 3 "~" H 4700 2600 50  0001 C CNN
	1    4700 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR06
U 1 1 6000B607
P 7100 4000
F 0 "#PWR06" H 7100 3750 50  0001 C CNN
F 1 "GNDREF" H 7105 3827 50  0001 C CNN
F 2 "" H 7100 4000 50  0001 C CNN
F 3 "" H 7100 4000 50  0001 C CNN
	1    7100 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3550 7100 3650
Wire Wire Line
	4350 3050 4350 3200
$Comp
L Diode:B230 D1
U 1 1 6001477A
P 4500 2400
F 0 "D1" V 4450 2200 50  0000 L CNN
F 1 "B230A" V 4550 2100 50  0000 L CNN
F 2 "Diode_SMD:D_SMB" H 4500 2225 50  0001 C CNN
F 3 "http://www.jameco.com/Jameco/Products/ProdDS/1538777.pdf" H 4500 2400 50  0001 C CNN
	1    4500 2400
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR04
U 1 1 6001211E
P 4500 2650
F 0 "#PWR04" H 4500 2400 50  0001 C CNN
F 1 "GNDREF" H 4505 2477 50  0001 C CNN
F 2 "" H 4500 2650 50  0001 C CNN
F 3 "" H 4500 2650 50  0001 C CNN
	1    4500 2650
	1    0    0    -1  
$EndComp
$Comp
L pspice:INDUCTOR L1
U 1 1 6000FF6F
P 4050 2150
F 0 "L1" H 4050 2365 50  0000 C CNN
F 1 "10uH" H 4050 2300 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 4050 2150 50  0001 C CNN
F 3 "~" H 4050 2150 50  0001 C CNN
	1    4050 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R2
U 1 1 6000F65A
P 4350 3300
F 0 "R2" H 4200 3450 50  0000 L CNN
F 1 "10k Ω" H 4000 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4350 3300 50  0001 C CNN
F 3 "~" H 4350 3300 50  0001 C CNN
	1    4350 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 6000DC1E
P 3250 2250
F 0 "C2" H 3350 2200 50  0000 L CNN
F 1 "22uF" H 3300 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 3250 2250 50  0001 C CNN
F 3 "~" H 3250 2250 50  0001 C CNN
	1    3250 2250
	-1   0    0    1   
$EndComp
$Comp
L power:GNDREF #PWR05
U 1 1 6000CCC7
P 4500 2850
F 0 "#PWR05" H 4500 2600 50  0001 C CNN
F 1 "GNDREF" H 4300 2800 50  0001 C CNN
F 2 "" H 4500 2850 50  0001 C CNN
F 3 "" H 4500 2850 50  0001 C CNN
	1    4500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3050 6950 3050
Wire Wire Line
	6950 3050 6950 3550
Wire Wire Line
	4500 2850 4600 2850
Wire Wire Line
	4600 2850 4600 2950
Wire Wire Line
	4600 2950 4700 2950
$Comp
L Device:R_Small_US R1
U 1 1 6005DE44
P 4000 3050
F 0 "R1" V 3800 3050 50  0000 L CNN
F 1 "52.3k Ω" V 3900 2950 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4000 3050 50  0001 C CNN
F 3 "~" H 4000 3050 50  0001 C CNN
	1    4000 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 3050 4700 3050
Connection ~ 7100 3550
$Comp
L Device:R_Small_US R3
U 1 1 6007B83E
P 7100 3350
F 0 "R3" H 7200 3450 50  0000 L CNN
F 1 "10k Ω" H 7200 3350 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7100 3350 50  0001 C CNN
F 3 "~" H 7100 3350 50  0001 C CNN
	1    7100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3650 7350 3650
$Comp
L Device:C_Small C4
U 1 1 60096628
P 8050 3200
F 0 "C4" H 7900 3150 50  0000 L CNN
F 1 "0.1uF" H 7800 3300 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 8050 3200 50  0001 C CNN
F 3 "~" H 8050 3200 50  0001 C CNN
	1    8050 3200
	-1   0    0    1   
$EndComp
$Comp
L power:GNDREF #PWR02
U 1 1 600D2A85
P 3250 2500
F 0 "#PWR02" H 3250 2250 50  0001 C CNN
F 1 "GNDREF" H 3255 2327 50  0001 C CNN
F 2 "" H 3250 2500 50  0001 C CNN
F 3 "" H 3250 2500 50  0001 C CNN
	1    3250 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2350 3250 2500
$Comp
L Device:C_Small C1
U 1 1 600DFA39
P 2850 2250
F 0 "C1" H 2950 2200 50  0000 L CNN
F 1 "NC" H 2950 2300 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 2850 2250 50  0001 C CNN
F 3 "~" H 2850 2250 50  0001 C CNN
	1    2850 2250
	-1   0    0    1   
$EndComp
$Comp
L power:GNDREF #PWR01
U 1 1 600DFA3F
P 2850 2500
F 0 "#PWR01" H 2850 2250 50  0001 C CNN
F 1 "GNDREF" H 2855 2327 50  0001 C CNN
F 2 "" H 2850 2500 50  0001 C CNN
F 3 "" H 2850 2500 50  0001 C CNN
	1    2850 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 2150 3250 2150
Connection ~ 3250 2150
Wire Wire Line
	2600 2250 2600 2450
Wire Wire Line
	8350 3200 8350 3400
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 600E95BF
P 2400 2150
F 0 "J1" H 2508 2331 50  0000 C CNN
F 1 "VOUT" H 2300 2100 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2400 2150 50  0001 C CNN
F 3 "~" H 2400 2150 50  0001 C CNN
	1    2400 2150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 601314C0
P 2050 1450
F 0 "H2" H 2150 1496 50  0000 L CNN
F 1 "F" H 2150 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad_Via" H 2050 1450 50  0001 C CNN
F 3 "~" H 2050 1450 50  0001 C CNN
	1    2050 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 2700 4700 2850
Wire Wire Line
	4700 2150 4700 2500
Wire Wire Line
	4500 2250 4500 2150
Wire Wire Line
	4500 2150 4700 2150
Wire Wire Line
	4700 2150 6900 2150
Wire Wire Line
	6900 2150 6900 2850
Connection ~ 4700 2150
Wire Wire Line
	6950 3550 7100 3550
Wire Wire Line
	6900 2950 7100 2950
Wire Wire Line
	7100 3450 7100 3550
$Comp
L Mechanical:MountingHole H3
U 1 1 60176E86
P 2250 1450
F 0 "H3" H 2350 1496 50  0000 L CNN
F 1 "F" H 2350 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad_Via" H 2250 1450 50  0001 C CNN
F 3 "~" H 2250 1450 50  0001 C CNN
	1    2250 1450
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 60177486
P 1850 1450
F 0 "H1" H 1950 1496 50  0000 L CNN
F 1 "F" H 1950 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad_Via" H 1850 1450 50  0001 C CNN
F 3 "~" H 1850 1450 50  0001 C CNN
	1    1850 1450
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 60177780
P 2450 1450
F 0 "H4" H 2550 1496 50  0000 L CNN
F 1 "F" H 2550 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad_Via" H 2450 1450 50  0001 C CNN
F 3 "~" H 2450 1450 50  0001 C CNN
	1    2450 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 2150 3650 2150
Wire Wire Line
	3650 3050 3650 2150
Connection ~ 3650 2150
Wire Wire Line
	3650 2150 3800 2150
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 6003519C
P 7750 3550
F 0 "J2" H 7800 3750 50  0000 L CNN
F 1 "EN" H 7850 3350 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-03A_1x03_P2.54mm_Vertical" H 7750 3550 50  0001 C CNN
F 3 "~" H 7750 3550 50  0001 C CNN
	1    7750 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 3900 7100 3900
Wire Wire Line
	7350 3650 7350 3900
Wire Wire Line
	7100 3850 7100 3900
Wire Wire Line
	7100 3100 8050 3100
Wire Wire Line
	7100 2950 7100 3100
Connection ~ 7100 3100
Wire Wire Line
	7100 3100 7100 3200
Connection ~ 7100 3900
Wire Wire Line
	7100 3900 7100 4000
Wire Wire Line
	7550 3450 7550 3200
Wire Wire Line
	7550 3200 7100 3200
Connection ~ 7100 3200
Wire Wire Line
	7100 3200 7100 3250
Wire Wire Line
	8050 3300 8050 3400
Wire Wire Line
	7100 3550 7550 3550
Wire Wire Line
	8350 3100 8050 3100
Connection ~ 8050 3100
Wire Wire Line
	8350 3400 8050 3400
Wire Wire Line
	2850 2350 2850 2450
Wire Wire Line
	2600 2150 2850 2150
Connection ~ 2850 2150
Wire Wire Line
	2600 2450 2850 2450
Connection ~ 2850 2450
Wire Wire Line
	2850 2450 2850 2500
Wire Wire Line
	4300 2150 4500 2150
Connection ~ 4500 2150
Wire Wire Line
	4100 3050 4350 3050
Connection ~ 4350 3050
Wire Wire Line
	3650 3050 3900 3050
$Comp
L teste-rescue:ADP2301AUJZ-R7-2021-01-15_16-19-05 U1
U 1 1 60027972
P 4700 2850
F 0 "U1" H 5800 3100 60  0000 C CNN
F 1 "ADP2301AUJZ-R7" H 5850 2750 60  0000 C CNN
F 2 "footprints:ADP2301AUJZ-R2" H 5800 3090 60  0001 C CNN
F 3 "" H 4700 2850 60  0000 C CNN
	1    4700 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 6001A774
P 7100 3750
F 0 "R4" H 6850 3750 50  0000 L CNN
F 1 "1.8k Ω" H 6800 3650 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7100 3750 50  0001 C CNN
F 3 "~" H 7100 3750 50  0001 C CNN
	1    7100 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR07
U 1 1 60022DD2
P 8050 3450
F 0 "#PWR07" H 8050 3200 50  0001 C CNN
F 1 "GNDREF" H 8055 3277 50  0001 C CNN
F 2 "" H 8050 3450 50  0001 C CNN
F 3 "" H 8050 3450 50  0001 C CNN
	1    8050 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 6011F4A3
P 8550 3100
F 0 "J3" H 8522 3074 50  0000 R CNN
F 1 "Conn_01x02_Male" H 8522 2983 50  0000 R CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 8550 3100 50  0001 C CNN
F 3 "~" H 8550 3100 50  0001 C CNN
	1    8550 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8050 3400 8050 3450
Connection ~ 8050 3400
$EndSCHEMATC
